import React from 'react';
import { StatusBar, StyleSheet, View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './src/reducers';
import Routes from './src/Routes';
import { CompaniesProvider } from './src/contexts/companyContext';


class App extends React.Component {

  render() {
    const store = createStore(reducers, {}); 
    return (
      // <Provider store={store}>
      <CompaniesProvider>
        <View style={styles.container}>
          <StatusBar barStyle="dark-content" />
          <Routes />
        </View>
      </CompaniesProvider>
      // </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 30
  },
});

export default App;
