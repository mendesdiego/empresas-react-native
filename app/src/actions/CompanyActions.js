import { CURRENT_COMPANY_CHANGED } from "./types";

export const setCurrentCompany = ({ prop, value }) => {
  return {
      type: CURRENT_COMPANY_CHANGED,
      payload: { prop, value }
  };
};