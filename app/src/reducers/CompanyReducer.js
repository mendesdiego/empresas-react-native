import {
  CURRENT_COMPANY_CHANGED,
  COMPANY_FETCH_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  companies: [],
  currentCompany: {}
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

      case CURRENT_COMPANY_CHANGED:
          return {...state, [action.payload.prop]: action.payload.value };

      default:
          return state;
  }
};