import React, { Component } from 'react';

const CompaniesContext = React.createContext({});

class CompaniesProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentCompany: {},
      setCurrentCompany: this.setCurrentCompany.bind(this)
    };
  }

  setCurrentCompany(company) {
    this.setState({ currentCompany: company })
  }

  render() {
    return (
      <CompaniesContext.Provider value={this.state} >
        {this.props.children}
      </CompaniesContext.Provider>
    );
  }
}

export { CompaniesContext, CompaniesProvider };


