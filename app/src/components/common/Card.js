import React from 'react';
import { View } from 'react-native';

const Card = ({ children, containerStyle }) => {
    return (
        <View style={{  ...styles.containerStyle, ...containerStyle}}>
            {children}
        </View>
    );
};

const styles = {
    containerStyle: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#ddd',
        backgroundColor: 'white',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 1,
        marginHorizontal: 2,
        padding: 10
    }
}

export { Card };