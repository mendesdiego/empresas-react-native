import React, { PureComponent } from 'react'
import { Text, View, StyleSheet } from 'react-native'

class TextField extends PureComponent {
  render() {
    const { label, value } = this.props;
    return (
      <View flexDirection={ value && value.length > 100 ? 'column' : 'row'}>
        <Text style={styles.label}>{label}: </Text>
        <Text style={styles.value}>{value}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  label: {
    fontWeight: 'bold'
  },
  value: {
    fontWeight: 'normal',
    textAlign: 'justify'
  }
});

export { TextField };