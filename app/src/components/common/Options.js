import _ from 'lodash';
import React from 'react';
import { Picker, Text, View } from 'react-native';


const Options = ({ label, selectedValue, options, containerStyle, onChange }) => {
    const { inputStyle, labelStyle } = styles;

    return (
        <View style={{ ...styles.containerStyle, containerStyle }}>
            <Text style={labelStyle}>{label}:</Text>
            <Picker
                selectedValue={selectedValue}
                style={styles.pickerStyle}
                onValueChange={(itemValue, itemIndex) => {
                    setTimeout( () => onChange(itemValue), 10)
                }}
            >
                {_.map(options, option =>
                    <Picker.Item label={option.label} value={option.value} key={new Date().getTime()} />)
                }
            </Picker>
        </View>
    );
};

const styles = {
    pickerStyle: { 
        height: 50, 
        width: 100 ,
        flex: 4,
    },
    labelStyle: {
        fontSize: 14,
        paddingLeft: 20,
        flex: 1
    },
    containerStyle: {
        height: 40,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    }

};

export { Options };
