import React from 'react';
import { TextInput, View, Text } from 'react-native';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry, containerStyle }) => {
    const { inputStyle, labelStyle } = styles;

    return (
        <View style={{ ...styles.containerStyle, containerStyle }}>
            <Text style={labelStyle}>{label}:</Text>
            <TextInput
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                autoCorrect={false}
                style={inputStyle}
                value={value}
                onChangeText={onChangeText}
            />
        </View>
    );
};

const styles = {
    inputStyle: {
        color: '#000',
        paddingHorizontal: 10,
        fontSize: 14,
        lineHeight: 23,
        flex: 4,
        borderColor: '#ccc',
        borderRadius: 20,
        borderWidth: 1,
    },
    labelStyle: {
        fontSize: 14,
        paddingLeft: 20,
        flex: 1
    },
    containerStyle: {
        height: 40,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    }

};

export { Input };