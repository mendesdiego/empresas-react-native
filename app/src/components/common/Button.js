import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { Spinner } from './Spinner';

const Button = ({ onPress, children, text, loading }) => {
    const { buttonStyle, textStyle } = styles;

    return (
        <TouchableOpacity 
            onPress={onPress} 
            style={buttonStyle}
        >
            { loading ?
                <Spinner style={textStyle} size={35}/> : 
                <Text style={textStyle}>
                    {text}
                </Text>
            }
        </TouchableOpacity>
    );
};

const styles = {

    textStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },

    buttonStyle: {
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5
    }

};

export { Button };