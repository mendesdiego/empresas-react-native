import _ from 'lodash';
import React, { Component } from 'react';
import { FlatList, Image, Picker, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { withContext } from 'with-context';
import { setCurrentCompany } from '../actions';
import { Card, Input, Options } from '../components/common';
import { CompaniesContext } from '../contexts/companyContext';
import { service } from '../services/companiesService';
import { BASE_URL } from '../util/constants';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companies: [],
      nomeEmpresa: '',
      tipoEmpresa: null,
      tiposEmpresas: [ { label: 'Selecione', value: null },
        { label: 'Agro', value: 1 }, { label: 'Aviation', value: 2 }, { label: 'Biotech', value: 3 },
        { label: 'Eco', value: 4 }, { label: 'Ecommerce', value: 5 }, { label: 'Education', value: 6 }, { label: 'Fashion', value: 7 },
        { label: 'Fintech', value: 8 }, { label: 'Food', value: 9 }, { label: 'Games', value: 10 }, { label: 'Health', value: 11 },
        { label: 'IOT', value: 12 }, { label: 'Logistics', value: 13 }, { label: 'Media', value: 14 }, { label: 'Mining', value: 15 },
        { label: 'Products', value: 16 }, { label: 'Real Estate', value: 17 }, { label: 'Service', value: 18 }, { label: 'Smart City', value: 19 },
        { label: 'Social', value: 20 }, { label: 'Software', value: 21 }, { label: 'Technology', value: 22 }, { label: 'Tourism', value: 23 }, { label: 'Transport', value: 24 }
      ]
    }
  }

  componentDidMount() {
    const sucessCallback = response => {
      this.setState({ companies: response.data.enterprises });
    };

    const errorCallback = error => console.log(error);

    service.findAll(sucessCallback, errorCallback)
  }

  onCardPress(company) {
    const { setCurrentCompany } = this.props.companiesContext;
    setCurrentCompany(company);
    this.props.navigation.navigate('Details');
  }

  renderCompanyCard({ item }) {
    const company = item;
    return (
      <TouchableOpacity onPress={() => this.onCardPress(company)}>
        <Card>
          {company.photo ?
            <Image source={{ uri: `${BASE_URL}${company.photo}` }} style={styles.cardImageStyle} /> :
            <Image source={require('../../assets/no-image.png')} style={styles.cardImageStyle} />
          }
          <Text style={{ textAlign: 'center', fontSize: 15 }}>{item.enterprise_name}</Text>
          <Text style={{ fontSize: 12 }}>{item.enterprise_name}</Text>
        </Card>
      </TouchableOpacity>

    )
  }

  renderCards() {
    const { companies } = this.state;

    return (
      <ScrollView style={styles.scrollViewContainer}>
        <FlatList
          data={companies}
          renderItem={this.renderCompanyCard.bind(this)}
          keyExtractor={(company) => `company-${company.id}`}
        />
      </ScrollView>
    )
  }

  onChangeFilters() {
    const { nomeEmpresa, tipoEmpresa } = this.state;
    const sucessCallback = response => this.setState({ companies: response.data.enterprises });
    const errorCallback = error => console.log(error);
    service.findByFilters(nomeEmpresa, tipoEmpresa, sucessCallback, errorCallback);
  }

  render() {
    const { nomeEmpresa, tipoEmpresa, tiposEmpresas } = this.state;
    return (
      <View style={styles.container}>
        <Text>Filtros:</Text>
        <View style={{ flexDirection: 'row' }}>
          <Input
            placeholder="Digite o nome..."
            label="Nome da Empresa"
            onChangeText={value => {
              this.setState({ nomeEmpresa: value });
              this.onChangeFilters();
            }}
            value={nomeEmpresa}
            containerStyle={{ flex: 2 }}
          />
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Options
            label="Tipo Empresa"
            selectedValue={tipoEmpresa}
            options={tiposEmpresas}
            onChange={(value) => {
              this.setState({ tipoEmpresa: value });
              this.onChangeFilters();
            }}/>
      </View>

        { this.renderCards() }

      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    flex: 1,
    padding: 5
  },
  scrollViewContainer: {
    flexDirection: 'column',
    flex: 1
  },
  cardContainer: {
    flexDirection: 'column'
  },
  cardImageStyle: {
    height: 100,
    width: '100%',
    flex: 1
  }
});

const mapStateToProps = (state) => {
  const { currentCompany } = state.company;
  return { currentCompany };
};

const mapDispatchToProps = { setCurrentCompany };

// TODO: Buscar motivos de erro do redux com react navigation. Utilizando contexto
// para deixar funcional.
const HomeConnected = connect(mapStateToProps, mapDispatchToProps)(Home);


export default withContext(CompaniesContext, 'companiesContext')(Home);