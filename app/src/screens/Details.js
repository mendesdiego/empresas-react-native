import _ from 'lodash';
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Card, TextField } from '../components/common';
import { service } from '../services/companiesService';
import { BASE_URL } from '../util/constants';
import { CompaniesContext } from '../contexts/companyContext';
import { withContext } from 'with-context';


class Details extends Component {

  constructor(props) {
    super(props);
    this.state = {
      company: {}
    }
  }

  componentDidMount() {
    const { currentCompany } = this.props.companiesContext;

    const sucessCallback = response => {
      this.setState({ company: response.data.enterprise });
    }
    const errorCallback = error => console.log(error);

    service.findById(currentCompany.id, sucessCallback, errorCallback);
  }

  render() {
    const { company } = this.state;
    return (
      <View style={styles.container}>
        <Card containerStyle={styles.cardContainer}>
          {company.photo ?
            <Image source={{ uri: `${BASE_URL}${company.photo}` }} style={styles.cardImageStyle} /> :
            <Image source={require('../../assets/no-image.png')} style={styles.cardImageStyle} />
          }
          <TextField label={'Nome da Empresa'} value={company.enterprise_name}/>
          <TextField label={'Tipo de Empresa'} value={_.get(company, 'enterprise_type.enterprise_type_name') || '-'}/>
          <TextField label={'Local'} value={`${company.city} / ${company.country}`}/>
          <TextField label={'Descrição'} value={company.description || '-'}/>
        </Card>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
  },
  cardContainer: {
    flexDirection: 'column',
    width: '98%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 1,
    padding: 5
  },
  cardImageStyle: {
    height: 200,
    width: '100%',
    borderRadius: 5,
  }
});

export default withContext(CompaniesContext, 'companiesContext')(Details);