import React, { Component } from 'react';
import { Image, KeyboardAvoidingView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Button, Card, Input, Spinner } from '../components/common';
import { service } from '../services/companiesService';
import { BACKGROUND_THEME_COLOR } from '../util/constants';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      companies: [],
      loading: false,
      invalidEmailOrPassword: false
    }
  }

  onButtonPress() {
    const { email, password } = this.state;
    this.setState({ loading: true, invalidEmailOrPassword: false });
    const onSuccess = () => {
      this.props.navigation.navigate('Home');
      this.setState({ invalidEmailOrPassword: false, loading: false })
    }
    const onError = (error) => {
      console.log(error);
      this.setState({ invalidEmailOrPassword: true, loading: false });
    }

    service.login(email, password, onSuccess, onError);
    
  }

  render() {
    const { email, password, loading } = this.state;
    return (
      <ScrollView 
        style={styles.scroolViewContainer} 
        keyboardShouldPersistTaps='aways'
      >
        <KeyboardAvoidingView
          behavior="position"
          keyboardVerticalOffset={20}
          enabled
        >
          <View style={styles.container}>
            <Image source={require('../../assets/logo.png')} style={styles.logoStyle} />
            <Card containerStyle={styles.cardContainer}>
              <Input
                placeholder="exemplo@email.com"
                label="Email"
                value={email}
                onChangeText={value => this.setState({ email: value })}
              />
              <Input
                placeholder="Senha"
                label="Senha"
                value={password}
                onChangeText={value => this.setState({ password: value })}
                secureTextEntry
              />
              <Button onPress={this.onButtonPress.bind(this)} text="Entrar" loading={loading}/>
              
              { this.state.invalidEmailOrPassword ? 
                <Text style={styles.errorLabel}>Email ou senha inválidos.</Text> : 
                null
              }
            </Card>
          </View>
        </KeyboardAvoidingView>

      </ScrollView>

    )
  }
}

const styles = StyleSheet.create({
  scroolViewContainer: {
    flex: 1,
    backgroundColor: BACKGROUND_THEME_COLOR,
  },
  logoStyle: {
    flex: 2,
    width: 300,
    height: 350
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 30,
    width: '100%',
    height: '100%'
    // alignItems: 'flex-start',
    // justifyContent: 'flex-start',
  },
  cardContainer: {
    width: '98%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }, 
  errorLabel: {
    color: 'red',
    textAlign: 'center',
    width: '100%'
  }
});

export default Login;