import Axios from 'axios';

class CompanyService {

  constructor() {

    this.headers = { 'Content-Type': 'application/json' };

    this.axios = Axios.create({
      baseURL: 'http://empresas.ioasys.com.br/api/v1/',
      headers: this.headers
    });
  }

  setCredentials(accessToken, client, uid) {
    this.headers = {
      'Content-Type': 'application/json',
      'access-token': accessToken,
      'client': client,
      'uid': uid
    }
  }

  login(email, password, successCallback, errorCallback) {
    return this.axios.post('users/auth/sign_in', { email, password })
      .then(response => {
        this.setCredentials(
          response.headers['access-token'],
          response.headers['client'],
          response.headers['uid'],
        )
        successCallback(response);
      })
      .catch(errorCallback)
  }

  findAll(successCallback, errorCallback) {
    return this.axios.get('enterprises', { headers: this.headers })
      .then(successCallback)
      .catch(errorCallback)
  }

  findByFilters(enterpriseName, enterprisesTypes, successCallback, errorCallback) {

    if (!enterpriseName && !enterprisesTypes) {
      return this.findAll(successCallback, errorCallback);
    }

    return this.axios.get('enterprises', {
      params: {
        enterprise_types: enterprisesTypes,
        name: enterpriseName
      },
      headers: this.headers
    })
      .then(successCallback)
      .catch(errorCallback)
  }

  findById(companyId, successCallback, errorCallback) {
    return this.axios.get(`enterprises/${companyId}`, { headers: this.headers })
      .then(successCallback)
      .catch(errorCallback)
  }
}

const service = new CompanyService();

export { service };