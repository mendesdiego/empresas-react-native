import React from "react";
import { Image, Platform, Text, View } from "react-native";
import { createAppContainer, createStackNavigator } from "react-navigation";
import Details from "./screens/Details";
import Home from "./screens/Home";
import Login from "./screens/Login";
import { Button } from "./components/common";

const navigationOptions = {
  headerStyle: {
    backgroundColor: '#333',
    height: Platform.OS === 'ios' ? 60 : 65
  },
  headerTintColor: 'white',
  headerTitle: (
    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
      <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
        <Image
          style={{ height: 60, width: 60 }}
          source={require('../assets/logo.png')}
        />
        <Text style={{ color: 'white', marginHorizontal: 10, fontSize: 20 }}>Empresas App</Text>
      </View>
    </View>
  )
}


const AppNavigator = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: { ...navigationOptions, headerLeft: null }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null,
    }
  },
  Details: {
    screen: Details,
    navigationOptions
  }
},
  {
    initialRouteName: 'Login'
  }
);

export default createAppContainer(AppNavigator);